using System.Collections.Generic;
using CodeBase.Components.Cell;
using CodeBase.Components.Grid;
using CodeBase.Components.Sign;
using CodeBase.Services;
using CodeBase.Systems;
using Leopotam.Ecs;
using NUnit.Framework;
using UnityEngine;

namespace Editor
{
    [TestFixture]
    public class GameLogicTests
    {
        [Test]
        public void CheckSanity()
        {
            EcsWorld world = new EcsWorld();

            Dictionary<Vector2Int, EcsEntity> cells = Instantiate3X3Grid(world);

            int chainLength1 = GameExtensions.GetLongestChain(cells, Vector2Int.zero);
            Assert.AreEqual(0, chainLength1);
            
            cells[Vector2Int.zero].Get<Taken>().sign = SignType.Cross;
            
            chainLength1 = GameExtensions.GetLongestChain(cells, Vector2Int.zero);
            int chainLength0 = GameExtensions.GetLongestChain(cells, new Vector2Int(2, 2));
            
            Assert.AreEqual(1, chainLength1);
            Assert.AreEqual(0, chainLength0);
        }

        [Test]
        public void CheckHorizontalTwoLeft()
        {
            EcsWorld world = new EcsWorld();

            Dictionary<Vector2Int, EcsEntity> cells = Instantiate3X3Grid(world);

            cells[new Vector2Int(2, 0)].Get<Taken>().sign = SignType.Cross;
            cells[new Vector2Int(1, 0)].Get<Taken>().sign = SignType.Cross;
            
            int chainLength = GameExtensions.GetLongestChain(cells, new Vector2Int(2, 0));
            
            Assert.AreEqual(2, chainLength);
        }
        
        [Test]
        public void CheckHorizontalTwoRight()
        {
            EcsWorld world = new EcsWorld();

            Dictionary<Vector2Int, EcsEntity> cells = Instantiate3X3Grid(world);

            cells[new Vector2Int(2, 0)].Get<Taken>().sign = SignType.Cross;
            cells[new Vector2Int(1, 0)].Get<Taken>().sign = SignType.Cross;
            
            int chainLength = GameExtensions.GetLongestChain(cells, new Vector2Int(1, 0));
            
            Assert.AreEqual(2, chainLength);
        }
        
        [Test]
        public void CheckVerticalTwoUp()
        {
            EcsWorld world = new EcsWorld();

            Dictionary<Vector2Int, EcsEntity> cells = Instantiate3X3Grid(world);

            cells[new Vector2Int(2, 0)].Get<Taken>().sign = SignType.Cross;
            cells[new Vector2Int(2, 1)].Get<Taken>().sign = SignType.Cross;
            
            int chainLength = GameExtensions.GetLongestChain(cells, new Vector2Int(2, 0));
            
            Assert.AreEqual(2, chainLength);
        }
        
        [Test]
        public void CheckVerticalTwoDown()
        {
            EcsWorld world = new EcsWorld();

            Dictionary<Vector2Int, EcsEntity> cells = Instantiate3X3Grid(world);

            cells[new Vector2Int(2, 0)].Get<Taken>().sign = SignType.Cross;
            cells[new Vector2Int(2, 1)].Get<Taken>().sign = SignType.Cross;
            
            int chainLength = GameExtensions.GetLongestChain(cells, new Vector2Int(2, 1));
            
            Assert.AreEqual(2, chainLength);
        }
        
        [Test]
        public void CheckPositiveDiagonalThree()
        {
            EcsWorld world = new EcsWorld();

            Dictionary<Vector2Int, EcsEntity> cells = Instantiate3X3Grid(world);

            cells[new Vector2Int(0, 0)].Get<Taken>().sign = SignType.Cross;
            cells[new Vector2Int(1, 1)].Get<Taken>().sign = SignType.Cross;
            cells[new Vector2Int(2, 2)].Get<Taken>().sign = SignType.Cross;
            
            int chainLength = GameExtensions.GetLongestChain(cells, new Vector2Int(1, 1));
            
            Assert.AreEqual(3, chainLength);
        }
        
        [Test]
        public void CheckNegativeDiagonalThree()
        {
            EcsWorld world = new EcsWorld();

            Dictionary<Vector2Int, EcsEntity> cells = Instantiate3X3Grid(world);

            cells[new Vector2Int(0, 2)].Get<Taken>().sign = SignType.Cross;
            cells[new Vector2Int(1, 1)].Get<Taken>().sign = SignType.Cross;
            cells[new Vector2Int(2, 0)].Get<Taken>().sign = SignType.Cross;
            
            int chainLength = GameExtensions.GetLongestChain(cells, new Vector2Int(1, 1));
            
            Assert.AreEqual(3, chainLength);
        }

        private static EcsEntity CreateCell(EcsWorld world, Vector2Int position)
        {
            EcsEntity entity = world.NewEntity();
            entity.Get<Position>().Value = position;
            entity.Get<Cell>();

            return entity;
        }

        private static Dictionary<Vector2Int, EcsEntity> Instantiate3X3Grid(EcsWorld world)
        {
            return new Dictionary<Vector2Int, EcsEntity>()
            {
                {new Vector2Int(0, 0), CreateCell(world, new Vector2Int(0, 0))},
                {new Vector2Int(0, 1), CreateCell(world, new Vector2Int(0, 1))},
                {new Vector2Int(0, 2), CreateCell(world, new Vector2Int(0, 2))},
                {new Vector2Int(1, 0), CreateCell(world, new Vector2Int(1, 0))},
                {new Vector2Int(1, 1), CreateCell(world, new Vector2Int(1, 1))},
                {new Vector2Int(1, 2), CreateCell(world, new Vector2Int(1, 2))},
                {new Vector2Int(2, 0), CreateCell(world, new Vector2Int(2, 0))},
                {new Vector2Int(2, 1), CreateCell(world, new Vector2Int(2, 1))},
                {new Vector2Int(2, 2), CreateCell(world, new Vector2Int(2, 2))},
            };
        }
    }
}
