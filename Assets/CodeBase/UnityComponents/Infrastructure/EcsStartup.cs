using CodeBase.Components.Cell;
using CodeBase.Components.Events;
using CodeBase.Components.Sign;
using CodeBase.Systems;
using CodeBase.UnityComponents.StaticData.Game;
using Leopotam.Ecs;
using UnityEngine;

namespace CodeBase.UnityComponents.Infrastructure
{
    sealed class EcsStartup : MonoBehaviour
    {
        private EcsWorld _world;
        private EcsSystems _systems;

        public Configuration configuration;
        public SceneData sceneData;

        private void Start()
        {
            // void can be switched to IEnumerator for support coroutines.

            _world = new EcsWorld();
            _systems = new EcsSystems(_world);
#if UNITY_EDITOR
            Leopotam.Ecs.UnityIntegration.EcsWorldObserver.Create(_world);
            Leopotam.Ecs.UnityIntegration.EcsSystemsObserver.Create(_systems);
#endif
            GameState gameState = new GameState {CurrentSign = SignType.Naught};

            _systems
                // register your systems here, for example:
                .Add(new InitializeFieldSystem())
                .Add(new CreateCellViewSystem())
                .Add(new SetCameraSystem())
                .Add(new ControlSystem())
                .Add(new AnalyzeClickSystem())
                .Add(new CreateTakenViewSystem())
                .Add(new CheckWinSystem())
                .Add(new WinSystem())
                .Add(new DrawSystem())
                .Add(new CurrentSignSystem())
                // .Add (new TestSystem2 ())

                // register one-frame components (order is important), for example:
                .OneFrame<Clicked>()
                .OneFrame<CheckWinEvent>()
                .OneFrame<CurrentSignEvent>()

                // inject service instances here (order doesn't important), for example:
                // .Inject (new CameraService ())
                // .Inject (new NavMeshSupport ())
                .Inject(configuration)
                .Inject(sceneData)
                .Inject(gameState)
                .Init();
        }

        private void Update() => _systems?.Run();

        private void OnDestroy()
        {
            if (_systems != null)
            {
                _systems.Destroy();
                _systems = null;
                _world.Destroy();
                _world = null;
            }
        }
    }
}