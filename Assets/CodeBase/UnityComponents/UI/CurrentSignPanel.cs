﻿using CodeBase.Components.Sign;
using TMPro;
using UnityEngine;

namespace CodeBase.UnityComponents.UI
{
    public class CurrentSignPanel : MonoBehaviour
    {
        public TextMeshProUGUI currentSignText;

        public void SetSign(SignType currentSign)
        {
            switch (currentSign)
            {
                case SignType.Cross:
                    currentSignText.text = "Крестик";
                    break;
                
                case SignType.Naught:
                    currentSignText.text = "Нолик";
                    break;
            }
        }
    }
}