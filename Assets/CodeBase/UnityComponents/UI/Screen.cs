﻿using UnityEngine;

namespace CodeBase.UnityComponents.UI
{
    public class Screen : MonoBehaviour
    {
        public void Show(bool state)
        {
            gameObject.SetActive(state);
        }
    }
}