﻿using UnityEngine.SceneManagement;

namespace CodeBase.UnityComponents.UI
{
    public class DrawScreen : Screen
    {
        public void OnRestartClick()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}