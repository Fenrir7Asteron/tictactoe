﻿using UnityEngine;

namespace CodeBase.UnityComponents.UI
{
    public class UI : MonoBehaviour
    {
        public WinScreen winScreen;
        public DrawScreen drawScreen;
        public CurrentSignPanel CurrentSign;
    }
}