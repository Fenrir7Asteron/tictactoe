﻿using System;
using CodeBase.Components.Sign;
using TMPro;
using UnityEngine.SceneManagement;

namespace CodeBase.UnityComponents.UI
{
    public class WinScreen : Screen
    {
        public TextMeshProUGUI winnerText;

        public void SetWinner(SignType winnerSign)
        {
            switch (winnerSign)
            {
                case SignType.Cross:
                    winnerText.text = "Крестик победил";
                    break;

                case SignType.Naught:
                    winnerText.text = "Нолик победил";
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(winnerSign), winnerSign, null);
            }
        }

        public void OnRestartClick()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}