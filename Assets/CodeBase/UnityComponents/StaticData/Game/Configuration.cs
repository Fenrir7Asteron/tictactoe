﻿using CodeBase.Systems;
using CodeBase.UnityComponents.Cell;
using UnityEngine;

namespace CodeBase.UnityComponents.StaticData.Game
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "StaticData/GameConfiguration")]
    public class Configuration : ScriptableObject
    {
        public int levelWidth = 3;
        public int levelHeight = 3;
        public int chainLength = 3;
        public CellView cellView;
        public Vector2 offset;
        public SignView NaughtView;
        public SignView CrossView;
    }
}