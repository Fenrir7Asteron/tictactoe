﻿using System.Collections.Generic;
using CodeBase.Components.Sign;
using Leopotam.Ecs;
using UnityEngine;

namespace CodeBase.UnityComponents
{
    public class GameState
    {
        public SignType CurrentSign;
        public readonly Dictionary<Vector2Int, EcsEntity> Cells =
            new Dictionary<Vector2Int, EcsEntity>();
    }
}