﻿using CodeBase.Systems;
using UnityEngine;

namespace CodeBase.UnityComponents
{
    public class SceneData : MonoBehaviour
    {
        public Transform cameraTransform;
        public Camera camera;
        public UI.UI ui;
    }
}