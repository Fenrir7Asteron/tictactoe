﻿using System.Collections.Generic;
using CodeBase.Components.Cell;
using CodeBase.Components.Sign;
using Leopotam.Ecs;
using UnityEngine;

namespace CodeBase.Services
{
    public static class GameExtensions
    {
        public static int GetLongestChain(this Dictionary<Vector2Int, EcsEntity> cells, Vector2Int startPosition)
        {
            EcsEntity startEntity = cells[startPosition];
            if (!startEntity.Has<Taken>())
            {
                return 0;
            }

            Vector2Int[] horizontalDirections = 
            {
                new Vector2Int(-1, 0),
                new Vector2Int(1, 0),
            };

            Vector2Int[] verticalDirections = 
            {
                new Vector2Int(0, -1),
                new Vector2Int(0, 1),
            };
            
            Vector2Int[] positiveDiagonalDirections = 
            {
                new Vector2Int(1, 1),
                new Vector2Int(-1, -1),
            };
            
            Vector2Int[] negativeDiagonalDirections = 
            {
                new Vector2Int(-1, 1),
                new Vector2Int(1, -1),
            };

            SignType startType = startEntity.Ref<Taken>().Unref().sign;
            
            int horizontalChainLength = ChainLength(cells, startPosition, horizontalDirections, startType);
            int verticalChainLength = ChainLength(cells, startPosition, verticalDirections, startType);
            int positiveDiagonalChainLength = ChainLength(cells, startPosition, positiveDiagonalDirections, startType);
            int negativeDiagonalChainLength = ChainLength(cells, startPosition, negativeDiagonalDirections, startType);

            return Mathf.Max(horizontalChainLength, verticalChainLength,
                positiveDiagonalChainLength, negativeDiagonalChainLength);
        }

        private static int ChainLength(Dictionary<Vector2Int, EcsEntity> cells, Vector2Int position, Vector2Int[] directions,
            SignType startType)
        {
            int verticalChainLength = 1;
            foreach (Vector2Int direction in directions)
            {
                Vector2Int currentPosition = position + direction;

                while (cells.TryGetValue(currentPosition, out EcsEntity entity))
                {
                    if (!entity.Has<Taken>())
                    {
                        break;
                    }

                    SignType type = entity.Ref<Taken>().Unref().sign;

                    if (type != startType)
                    {
                        break;
                    }

                    currentPosition += direction;
                    verticalChainLength++;
                }
            }

            return verticalChainLength;
        }
    }
}