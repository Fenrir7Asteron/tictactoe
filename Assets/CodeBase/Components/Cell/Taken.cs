﻿using CodeBase.Components.Sign;

namespace CodeBase.Components.Cell
{
    public struct Taken
    {
        public SignType sign;
    }
}