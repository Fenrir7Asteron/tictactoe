﻿using CodeBase.UnityComponents.Cell;

namespace CodeBase.Components.Cell
{
    public struct TakenRef
    {
        public SignView signView;
    }
}