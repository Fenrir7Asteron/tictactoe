﻿using CodeBase.UnityComponents;
using CodeBase.UnityComponents.Cell;

namespace CodeBase.Components.Cell
{
    public struct CellViewRef
    {
        public CellView CellView;
    }
}