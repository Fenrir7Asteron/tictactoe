﻿using UnityEngine;

namespace CodeBase.Components.Grid
{
    public struct Position
    {
        public Vector2Int Value;
    }
}