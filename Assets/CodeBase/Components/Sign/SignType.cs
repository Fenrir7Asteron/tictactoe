﻿namespace CodeBase.Components.Sign
{
    public enum SignType
    {
        None = -1,
        Naught = 0,
        Cross = 1,
    }
}