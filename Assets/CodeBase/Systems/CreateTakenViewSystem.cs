﻿using System;
using CodeBase.Components.Cell;
using CodeBase.Components.Sign;
using CodeBase.UnityComponents.Cell;
using CodeBase.UnityComponents.StaticData.Game;
using Leopotam.Ecs;
using UnityEngine;
using Object = System.Object;

namespace CodeBase.Systems
{
    public class CreateTakenViewSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Taken, CellViewRef>.Exclude<TakenRef> _filter = null;
        private readonly Configuration _configuration = null;

        public void Run()
        {
            foreach (int index in _filter)
            {
                Vector3 position = _filter.Get2(index).CellView.transform.position;
                SignType takenSign = _filter.Get1(index).sign;

                SignView signView = null;
                switch (takenSign)
                {
                    case SignType.Naught:
                        signView = _configuration.NaughtView;
                        break;
                        
                    case SignType.Cross:
                        signView = _configuration.CrossView;
                        break;
                    
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                
                SignView instance = UnityEngine.Object.Instantiate(signView, position, Quaternion.identity);
                _filter.GetEntity(index).Get<TakenRef>().signView = instance;
            }
        }
    }
}