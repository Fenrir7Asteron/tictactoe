﻿using CodeBase.Components.Cell;
using CodeBase.Components.Grid;
using CodeBase.UnityComponents;
using CodeBase.UnityComponents.Cell;
using CodeBase.UnityComponents.StaticData.Game;
using Leopotam.Ecs;
using UnityEngine;

namespace CodeBase.Systems
{
    public class CreateCellViewSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Cell, Position>.Exclude<CellViewRef> _filter = null;
        private readonly Configuration _configuration = null;
        
        public void Run()
        {
            foreach (int index in _filter)
            {
                ref Position position = ref _filter.Get2(index);

                CellView cellView = Object.Instantiate(_configuration.cellView);

                cellView.transform.position = new Vector3(
                    position.Value.x * (1.0f + _configuration.offset.x),
                    position.Value.y * (1.0f + _configuration.offset.y));

                EcsEntity cellViewEntity = _filter.GetEntity(index);

                cellView.Entity = cellViewEntity;
                
                cellViewEntity.Get<CellViewRef>().CellView = cellView;
            }
        }
    }
}