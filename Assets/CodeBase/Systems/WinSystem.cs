﻿using CodeBase.Components.Cell;
using CodeBase.UnityComponents;
using Leopotam.Ecs;

namespace CodeBase.Systems
{
    public class WinSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Winner, Taken> _filter = null;
        private readonly SceneData _sceneData = null;
        
        public void Run()
        {
            if (!_sceneData.ui.winScreen.gameObject.activeInHierarchy)
            {
                foreach (int index in _filter)
                {
                    ref Taken winnerType = ref _filter.Get2(index);

                    _sceneData.ui.winScreen.Show(true);
                    _sceneData.ui.winScreen.SetWinner(winnerType.sign);
                }
            }
        }
    }
}