﻿using CodeBase.Components.Cell;
using CodeBase.Components.Events;
using CodeBase.UnityComponents;
using Leopotam.Ecs;

namespace CodeBase.Systems
{
    public class CurrentSignSystem : IEcsRunSystem
    {
        private readonly EcsFilter<CurrentSignEvent> _filter = null;
        
        private readonly SceneData _sceneData = null;
        private readonly GameState _gameState = null;
        
        public void Run()
        {
            if (!_filter.IsEmpty())
            {
                _sceneData.ui.CurrentSign.SetSign(_gameState.CurrentSign);
            }
        }
    }
}