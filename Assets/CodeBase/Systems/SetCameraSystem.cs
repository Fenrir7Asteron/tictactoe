﻿using CodeBase.UnityComponents;
using CodeBase.UnityComponents.StaticData.Game;
using Leopotam.Ecs;
using UnityEngine;

namespace CodeBase.Systems
{
    public class SetCameraSystem : IEcsInitSystem
    {
        private readonly SceneData _sceneData = null;
        private readonly Configuration _configuration = null;
        
        public void Init()
        {
            Camera camera = _sceneData.camera;
            camera.orthographic = true;
            
            int width = _configuration.levelWidth;
            int height = _configuration.levelHeight;
            float offsetX = _configuration.offset.x;
            float offsetY = _configuration.offset.y;
            camera.orthographicSize = height / 2f + (height - 1) * offsetY / 2f;

            _sceneData.cameraTransform.position = new Vector3(
                width / 2f + (width - 1) * offsetX / 2f,
                height / 2f + (height - 1) * offsetY / 2f);
        }
    }
}