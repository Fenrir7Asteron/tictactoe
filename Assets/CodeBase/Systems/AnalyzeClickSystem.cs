﻿using CodeBase.Components.Cell;
using CodeBase.Components.Events;
using CodeBase.Components.Grid;
using CodeBase.Components.Sign;
using CodeBase.UnityComponents;
using Leopotam.Ecs;

namespace CodeBase.Systems
{
    public class AnalyzeClickSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Cell, Clicked>.Exclude<Taken> _filter = null;
        private readonly GameState _gameState = null;

        public void Run()
        {
            foreach (int index in _filter)
            {
                ref EcsEntity ecsEntity = ref _filter.GetEntity(index);
                ecsEntity.Get<Taken>().sign = _gameState.CurrentSign;
                ecsEntity.Get<CheckWinEvent>();
                ecsEntity.Get<CurrentSignEvent>();

                _gameState.CurrentSign =
                    _gameState.CurrentSign == SignType.Naught ? SignType.Cross : SignType.Naught;
            }
        }
    }
}