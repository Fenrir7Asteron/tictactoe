﻿using CodeBase.Components.Cell;
using CodeBase.Components.Events;
using CodeBase.Components.Grid;
using CodeBase.Components.Sign;
using CodeBase.Services;
using CodeBase.UnityComponents;
using CodeBase.UnityComponents.StaticData.Game;
using Leopotam.Ecs;

namespace CodeBase.Systems
{
    public class CheckWinSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Position, Taken, CheckWinEvent> _filter = null;
        private readonly GameState _gameState = null;
        private readonly Configuration _configuration = null;

        public void Run()
        {
            foreach (int index in _filter)
            {
                ref Position position = ref _filter.Get1(index);
                ref Taken type = ref _filter.Get2(index);

                int chainLength = _gameState.Cells.GetLongestChain(position.Value);

                if (chainLength >= _configuration.chainLength)
                {
                    _filter.GetEntity(index).Get<Winner>();
                }
            }
        }
    }
}