﻿using CodeBase.Components.Cell;
using CodeBase.Components.Grid;
using CodeBase.UnityComponents;
using Leopotam.Ecs;
using UnityEngine;

namespace CodeBase.Systems
{
    public class DrawSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Cell>.Exclude<Taken> _freeCells = null;
        private readonly EcsFilter<Winner> _winner = null;
        private readonly SceneData _sceneData = null;
        
        public void Run()
        {
            if (_freeCells.IsEmpty() && _winner.IsEmpty() && !_sceneData.ui.drawScreen.gameObject.activeInHierarchy)
            {
                _sceneData.ui.drawScreen.Show(true);
            }
        }
    }
}