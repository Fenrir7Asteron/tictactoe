﻿using CodeBase.Components.Events;
using CodeBase.Components.Grid;
using CodeBase.UnityComponents;
using CodeBase.UnityComponents.StaticData.Game;
using Leopotam.Ecs;
using UnityEngine;

namespace CodeBase.Systems
{
    public class InitializeFieldSystem : IEcsInitSystem
    {
        private readonly Configuration _configuration = null;
        private readonly EcsWorld _world = null;
        private readonly GameState _gameState = null;
        
        public void Init()
        {
            for (int x = 0; x < _configuration.levelWidth; ++x)
            {
                for (int y = 0; y < _configuration.levelHeight; ++y)
                {
                    EcsEntity cellEntity = _world.NewEntity();
                    cellEntity.Get<Cell>();
                    
                    Vector2Int position = new Vector2Int(x, y);
                    cellEntity.Get<Position>().Value = position;

                    _gameState.Cells[position] = cellEntity;
                }
            }

            _world.NewEntity().Get<CurrentSignEvent>();
        }
    }
}