﻿using CodeBase.Components.Cell;
using CodeBase.UnityComponents;
using CodeBase.UnityComponents.Cell;
using Leopotam.Ecs;
using UnityEngine;

namespace CodeBase.Systems
{
    public class ControlSystem : IEcsRunSystem
    {
        private readonly SceneData _sceneData = null;

        public void Run()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Camera camera = _sceneData.camera;
                Ray ray = camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out RaycastHit hitInfo))
                {
                    CellView cellView = hitInfo.collider.GetComponent<CellView>();
                    if (cellView)
                    {
                        cellView.Entity.Get<Clicked>();
                    }
                }
            }
        }
    }
}